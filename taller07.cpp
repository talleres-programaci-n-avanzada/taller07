#include <iostream>
#include <string>

using namespace std;

string desencriptar(int tam, string cadena, string desplazamientos, string direcciones);

int main(void) {
    string cadena = "Verognab PBsdil";
    string desplazamientos = "jinbjcbfhedgicfefeccilcffblngidligjb";
    string direcciones = "101000110111010101110100110100110100";
    
    int tam = desplazamientos.size();
    
    string mensaje = desencriptar(tam, cadena, desplazamientos, direcciones);
    cout << mensaje;
}

string desencriptar(int tam, string cadena, string desplazamientos, string direcciones){
    string mensaje = "";
    int desplazamiento = 0;
    char letra;
    for(int i=0; i<tam; i++){
        int nuevo;
        if (desplazamientos[i] >= 'a' && desplazamientos[i] <= 'z'){
            nuevo = desplazamientos[i]- 'a';
        }
        if (desplazamientos[i] >= 'A' && desplazamientos[i] <= 'Z'){
            nuevo = desplazamientos[i]- 'A';
        }
        
        if(direcciones[i]=='0'){
            if(desplazamiento - nuevo >= 0){
                desplazamiento -= nuevo;
            }
            else if (desplazamiento - nuevo < 0){
                desplazamiento = (desplazamiento - nuevo) + tam;
            }
        }
        if(direcciones[i]=='1'){
            if(desplazamiento + nuevo <=tam){
                desplazamiento += nuevo;
            }
            else if (desplazamiento - nuevo >tam){
                desplazamiento = (desplazamiento + nuevo)-tam;
            }
        }
        letra = cadena[desplazamiento];
        mensaje += letra;
    }
    return mensaje;
}

