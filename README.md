# taller07

En  la  empresa  de  construcciones  ABC,  los  ingenieros  civiles  mantienen  contratos  con muchas  empresas  para elaborar  diversas  obras.  Ellos  necesitan  transmitir  información entre  las  mismas  y  han  contratado  a  un  Ingeniero  Electrónico  para  que  invente  los dispositivos que controlarán la comunicación. Pero como la información es confidencial y es inseguro transmitir la información como texto normal, contratan a un Ingeniero Industrial para  que  planifique  la  estrategia  y  la  forma  como  debe  realizarse  la  comunicación.  Se reúnen y deciden codificar los mensajes de la siguiente manera. Primero enviarán las letras presentes en el mensaje sin importar el orden en que vayan (sin repetición),  luego  transmiten  un  arreglo  de  desplazamientos  y  finalmente  un  arreglo  de direcciones. A cada posición del vector de desplazamientos le corresponde como pareja el dato que está en la misma posición del vector de direcciones, y esta pareja finalmente será la  clave  de  descripción  del  mensaje:  El  vector  de  desplazamientos  indica  el  número  de caracteres a avanzar (‘a’:0, ‘b’:1, ‘c’:2, ... ‘z’:26) y el vector de direcciones indica hacia dónde va el desplazamiento (1: adelante 0: atrás). Para desencriptar el mensaje, se parte desde la primera letra de la cadena de símbolos y se  avanza  tantas  casillas  como  indique  el  vector  de  desplazamiento  en  la  dirección  que indique el vector de dirección. Por ejemplo, si llega un paquete de datos así: 

```text
    Cadena de letras presentes: Verognab PBsdil 
    Arreglo de desplazamientos: jinbjcbfhedgicfefeccilcffblngidligjb 
    Arreglo con las direcciones: 101000110111010101110100110100110100 
```

Se  toma  la  primera  posición  de los  arreglos  de  desplazamiento  y  de  dirección  (j,1),  que indica que se debe desplazar 9 posiciones hacia la derecha a partir del inicio de la cadena de letras presentes, obteniendo la letra P. Luego se procede a sacar los datos de la segunda posición  de  los  vectores  de  desplazamiento  y  dirección  (i,0),  indicando  que  se  debe desplazar 8 posiciones hacia la izquierda en la cadena de letras presentes, obteniendo la e. Se continúa de esta manera hasta el final de los arreglos de desplazamientos y direcciones para desenpcriptar todo el mensaje, dando como resultado la cadena: Peligro en la obra Brisas del Volador. 

Desencripte el  mensaje  y  deje  como  resultado  el  texto  de  salida, use  memoria  dinámica para crear la cadena resultado. 

## Condiciones que debe cumplir el programa
No puede utilizar la notación tradicional de vectores y/ matrices sino con apuntadores.